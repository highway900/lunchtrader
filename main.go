package main

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"net/url"
	"os"
	"os/signal"
	"time"

	"github.com/gorilla/websocket"
)

var addr = flag.String("addr", "localhost:8080", "http service address")

type Auth struct {
	Key       string `json:"key"`
	Signature string `json:"signature"`
	Timestamp string `json:"timestamp"`
}

// {"e":"ping","time":1498735184577}
type Ping struct {
	E    string `json:"e"`
	Time int32  `json:"time"`
}

func makeHMAC(message string, secret string) string {
	sig := hmac.New(sha256.New, []byte(secret))
	sig.Write([]byte(message))
	return hex.EncodeToString(sig.Sum(nil))
}

func makeTimestampString() string {
	return fmt.Sprintf("%d", int32(time.Now().Unix()))
}

func send(c *websocket.Conn, msg []byte) {
	log.Println("send:", string(msg))
	err := c.WriteMessage(websocket.TextMessage, msg)
	if err != nil {
		log.Println("err write:", err)
		return
	}
}

func read(c *websocket.Conn, done chan struct{}, handle chan string) {
	defer c.Close()
	defer close(done)
	for {
		_, message, err := c.ReadMessage()
		if err != nil {
			log.Println("read:", err)
			return
		}
		log.Printf("recv: %s", message)
		var parsed map[string]interface{}
		if err = json.Unmarshal(message, &parsed); err != nil {
			panic("message recv fail")
		}
		e, ok := parsed["e"].(string)
		if ok {
			handle <- e
		}
	}
}

func main() {
	flag.Parse()
	log.SetFlags(0)

	handler := make(chan string)
	interrupt := make(chan os.Signal, 1)
	signal.Notify(interrupt, os.Interrupt)

	secret := os.Getenv("CEX_API_SECRET")
	key := os.Getenv("CEX_API_KEY")

	if len(secret) == 0 || len(key) == 0 {
		panic("CEX API ENV vars not set")
	}

	timestamp := makeTimestampString()
	sig := makeHMAC(timestamp+key, secret)

	fmt.Println(timestamp)

	u := url.URL{Scheme: "wss", Host: *addr, Path: "ws"}
	log.Printf("connecting to %s", u.String())

	c, _, err := websocket.DefaultDialer.Dial(u.String(), nil)
	if err != nil {
		log.Fatal("dial:", err)
	}
	defer c.Close()

	done := make(chan struct{})

	go read(c, done, handler)

	// Login
	auth_req := struct {
		E    string `json:"e"`
		Auth Auth   `json:"auth"`
	}{
		"auth",
		Auth{key, sig, timestamp},
	}

	r, err := json.Marshal(auth_req)
	if err != nil {
		log.Println(err)
	}
	send(c, r)

	// Subscribe ticker
	tick := []byte(`{"e": "subscribe","rooms": ["tickers"]}`)
	send(c, tick)

	for {
		select {
		case e := <-handler:
			switch e {
			case "ping":
				_pong := struct {
					E string `json:"e"`
				}{
					"pong",
				}
				pong, err := json.Marshal(_pong)
				if err != nil {
					panic("unable to marshal json")
				}
				send(c, pong)
			}
			break
		case <-interrupt:
			log.Println("interrupt")
			// To cleanly close a connection, a client should send a close
			// frame and wait for the server to close the connection.
			err := c.WriteMessage(websocket.CloseMessage, websocket.FormatCloseMessage(websocket.CloseNormalClosure, ""))
			if err != nil {
				log.Println("write close:", err)
				return
			}
			select {
			case <-done:
			case <-time.After(time.Second):
			}
			c.Close()
			return
		}
	}
}
