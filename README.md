Lunch Trader
============

High Steaks Trading!

Use ETH trading to make lunch money and profit.

usage: `main -addr ws.cex.io`

Currently uses CEX.io [API](https://cex.io/websocket-api) and websockets

Need to set some ENV vars

`CEX_API_SECRET`
`CEX_API_KEY`

You can acquire these from your CEX.io account
